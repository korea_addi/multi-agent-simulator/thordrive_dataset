# Thordrive 2024 Dataset
Dataset Link : [link](http://database.ketiauto.kr:5000/index.cgi?launchApp=SYNO.SDS.App.FileStation3.Instance&launchParam=openfile%3D%252Fkorea_addi_d1%252Fthordrive%252Fdataset_2024%252F)

# Features
- Thordrive 2024 Dataset은 CARLA를 이용한 가상 도로환경의 데이터입니다.
- 총 22개의 scene으로 구성되며, 각 scene은 1개의 카메라 데이터와 오도메트리 정보를 포함합니다.
- ego_trajectory.json은 각 프레임에서의 차량의 6-DoF 글로벌 좌표와 오도메트리를 포함하며, x,y,z,odom 값은 meter, roll, pitch, yaw는 degree를 단위로 합니다.

# Dataset 구조
Thordrive 2024 Dataset은 아래와 같은 구조로 구성되어 있습니다.
```
korea_addi_d1/
├─ thordrive/
|   ├─ dataset_2024/
|   |   ├─ scene000/
|   |   |   ...
|   |   ├─ scene021/
|   |   |   ├─ CAM_FRONT/
|   |   |   |   ├─ 000000000.png
|   |   |   |   |   ...
|   |   |   ├─ ego_trajectory.json
|   |   ├─
|   ├─
|   |   ...
├─
