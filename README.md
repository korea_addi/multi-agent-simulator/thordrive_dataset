# Thordrive Dataset
Dataset Link:

#2022: [link](data_description_2022.md)

#2023: [link](data_description_2023.md)

#2024: [link](data_description_2024.md)

## 소개
Thordrive Dataset은 자율주행 알고리즘의 개발, 검증을 위해 개발된 시뮬레이터 [CARLA](https://carla.org/)를 이용하여 가상의 도로 주행환경에서 생성된 카메라 및 라이다의 센서값을 제공합니다.

